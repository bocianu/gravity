(* declare your interrupt routines here *)

procedure dli;assembler;interrupt;
asm {
    sta wsync           ;//
    phr ; store registers
    sta wsync           ;//
    lda #0
    ldx dlibackOffset
    sta colpm0
    sta hposm2
    sta wsync           ;//
    lda starsColor
    sta colpm2
    lda viewX
    sta wsync           ;//
    lsr:lsr
    sta xoff
    lda dliY
    sta yoff
    :3 sta wsync        ;//

dliloop               ;// vvv
    ldy vcount
    lda DLIBACK_ADDRESS, x
    inx
    sta wsync         ;//--------- WSYNC
    sta colbak
    
xoff = * + 1
    lda #0
    add STARS_ADDRESS,x
    sta hposm2

    lda #0
yoff = * + 1 
    cpy #0
    bmi @+
    lda #C_FIREDARK
@   
    sta wsync         ;//--------- WSYNC
    sta colpm0

    cpy #111
    bmi dliloop       ;// ^^^
    
    lda #0
    sta wsync
    sta colbak
    sta colpm2
enddli
    plr ; restore registers
};
end;

procedure vbl;assembler;interrupt;
asm {
    phr ; store registers
    
play_music     

    lda music_on 
    beq player_end

    lda MSX
    ldy MSX+1
    jsr RMT.TRMT.PLAY

player_end

    plr ; restore registers
    jmp $E462 ; jump to system VBL handler
};
end;
