for IMAGE in $(ls -1 _*.png)
do
    echo \*\*\* CONVERTING $IMAGE
    NAME="${IMAGE%.*}"
    magick convert $IMAGE[0] -depth 1 -colorspace gray $NAME.gray
    mv $NAME.gray $NAME.gfx
    ./rle.exe $NAME.gfx $NAME.rle 
done

