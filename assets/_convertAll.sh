#!/bin/bash
for IMAGE in $(ls -1 *tileset.png)
do
    echo \*\*\* CONVERTING $IMAGE
    NAME="${IMAGE%.*}"
    magick convert $IMAGE -depth 2 $NAME.gray
    python swapcolors2bpp.py $NAME.gray $NAME.gray1 0 1
#    python swapcolors2bpp.py $NAME.gray1 $NAME.gfx 1 2
    cp $NAME.gray1 $NAME.2bpp
    rm $NAME.gray* 

done

