    icl 'const.inc'
levels
;ptrs to levels
    dta a(level1)
    dta a(level2)
    dta a(level2)
    dta a(level2)
    dta a(level2)


level1
    dta BLK_CLEAR
    dta BLK_STARTOFF,21
    dta BLK_SETX,66
    dta BLK_SETPRIO,$21
    dta BLK_SETCHARS,>TILES1
    dta BLK_SETSTARS,%00000000
    dta BLK_SETBKSOLID,$0,18
    dta BLK_SETBKSOLID,$6a,2
    dta BLK_SETBKSOLID,$6c,3
    dta BLK_SETBKTRANS,$6c,$6e,2
    dta BLK_SETBKSOLID,$6e,4
    dta BLK_SETBKTRANS,$6e,$7e,2
    dta BLK_SETBKSOLID,$7E,6
    dta BLK_SETBKTRANS,$7e,$7c,2
    dta BLK_SETBKSOLID,$7C,23
    dta BLK_SETBKTRANS,$7c,$7a,6
    dta BLK_SETBKSOLID,$7A,28 ;// 96
    dta BLK_SETCOLORS,0,$fb,$f
    ;dta BLK_LOADRAW,a(landing)
    dta BLK_LOADRLE,a(landing)
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDTILE,a(cloud0),16
    dta BLK_SETBKTRANS,$7A,$78,14
    dta BLK_SETBKSOLID,$78,18
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDTILE,a(cloud0),8
    dta BLK_RNDTILE,a(cloud1),8
    dta BLK_SETSTARS,%00000001
    dta BLK_SETBKTRANS,$78,$76,14
    dta BLK_SETBKSOLID,$76,18
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDTILE,a(cloud0),8
    dta BLK_RNDTILE,a(cloud1),8
    dta BLK_RNDTILE,a(cloud2),8
    dta BLK_SETBKSOLID,$76,32
    dta BLK_END  ; -------------------------------------------------- ------ end 0


loop_point
    dta BLK_CLEAR
    dta BLK_SETCOLORS,$78,$7a,$0f
    dta BLK_CYCLECOLOR,5,40,a(710)
    dta $0e,$c,$0a,$08,$06
    dta BLK_RNDTILE,a(cloud0),4
    dta BLK_RNDTILE,a(cloud1),4
    dta BLK_RNDTILE,a(cloud2),4
    dta BLK_RNDTILE,a(cloud3),4
    dta BLK_SETBKTRANS,$76,$74,14
    dta BLK_SETBKSOLID,$74,18
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDTILE,a(cloud0),2
    dta BLK_RNDTILE,a(cloud1),2
    dta BLK_RNDTILE,a(cloud2),2
    dta BLK_RNDTILE,a(cloud3),2
    dta BLK_RNDTILE,a(cloud4),2
    dta BLK_SETSTARS,%00010001
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETPRIO,$24;
    dta BLK_RNDTILE,a(cloud0),3
    dta BLK_RNDTILE,a(cloud1),3
    dta BLK_RNDTILE,a(cloud2),3
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDTILE,a(cloud0),2
    dta BLK_RNDTILE,a(cloud1),2
    dta BLK_RNDTILE,a(cloud2),2
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; -------------------------------------------------- ------  end 

    dta BLK_CLEAR
    dta BLK_RNDTILE,a(cloud0),4
    dta BLK_RNDCHAR,CHAR_DUST,8
    dta BLK_RNDCHAR,CHAR_ALLOY2,8
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; --------------------------------------------------


    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,4
    dta BLK_RNDCHAR,CHAR_ALLOY2,4
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,2
    dta BLK_RNDCHAR,CHAR_ALLOY2,2
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; --------------------------------------------------


    dta BLK_CLEAR
    dta BLK_SETCHARS,>TILES2
    dta BLK_SETCOLORS,$76,$1a,$f
    dta BLK_CYCLECOLOR,4,6 + LOOP_CYCLE,a(710)
    dta $7e,$7c,$7a,$7c
    dta BLK_SETPRIO,$22
    dta BLK_RNDCHAR,CHAR_ALLOY2,4
    dta BLK_RNDCHAR,CHAR_STAR1,4
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; -------------------------------------------------- ------  end 


    ;dta BLK_JUMP, a(jmp1)


    dta BLK_CLEAR
    dta BLK_SETSTARS,%01010010
    dta BLK_RNDCHAR,CHAR_ALLOY2,2
    dta BLK_RNDCHAR,CHAR_STAR1,3
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; --------------------------------------------------


    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY2,4
    dta BLK_RNDCHAR,CHAR_STAR1,6
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_STAR2,8
    dta BLK_RNDCHAR,CHAR_ALLOY2,8
    dta BLK_PUTTILE,a(moon),29,14
    dta BLK_SETPRIO,$24;
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SLOWDOWN,80
    dta BLK_SETPRIO,$22
    dta BLK_SETBKSOLID,$74,32
    dta BLK_RNDCHAR,CHAR_STAR2,8
    dta BLK_RNDCHAR,CHAR_ALLOY2,8
    dta BLK_END  ; -------------------------------------------------- ------  end 


    dta BLK_CLEAR
    dta BLK_SETSTARS,%01010010
    dta BLK_RNDCHAR,CHAR_ALLOY2,5
    dta BLK_RNDCHAR,CHAR_STAR2,5
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; --------------------------------------------------


    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY2,3
    dta BLK_RNDCHAR,CHAR_STAR2,3
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_STAR2,2
    dta BLK_RNDCHAR,CHAR_ALLOY2,2
    dta BLK_SETPRIO,$24;
    dta BLK_SETBKSOLID,$74,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETPRIO,$22
    dta BLK_SETBKSOLID,$74,6
    dta BLK_SETBKTRANS,$74,$72,26
    dta BLK_END  ; -------------------------------------------------- ------  end 

    dta BLK_CLEAR
    dta BLK_PUTTILE,a(word_code),20,0
    dta BLK_PUTTILE,a(word_code),36,6
    dta BLK_PUTTILE,a(word_code),28,12
    dta BLK_SETSTARS,%01010010
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_CYCLECOLOR,8 + LOOP_CYCLE,2,a(710)
    dta $2c,$4c,$5c,$7c,$9c,$bc,$de,$fe
    dta BLK_SETCOLORS,$75,$fb,$1a    
    dta BLK_SETBKSOLID,$72,32
    dta BLK_RNDCHAR,CHAR_DUST,12
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,6
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; -------------------------------------------------- ------ end 

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------


    dta BLK_CLEAR
    dta BLK_PUTTILE,a(word_eat),24,0
    dta BLK_PUTTILE,a(word_eat),39,4
    dta BLK_PUTTILE,a(word_eat),20,8
    dta BLK_PUTTILE,a(word_eat),33,12
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETCOLORS,$78,$7a,$0f
    dta BLK_CYCLECOLOR,9 + LOOP_CYCLE,0,a(710)
    dta $00,$02,$00,$08,$0c,$02,$0a,$04,$02
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; ------------------------------------------------- -------  end 

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_STAR2,12    
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_STAR2,24   
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------


    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_STAR2,8   
    dta BLK_RNDCHAR,CHAR_ALLOY2,8   
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; -------------------------------------------------- -------  end 


    dta BLK_CLEAR
    dta BLK_PUTTILE,a(word_compete),34,0
    dta BLK_PUTTILE,a(word_compete),18,5
    dta BLK_PUTTILE,a(word_compete),23,11
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,40,12   
    dta BLK_CYCLECOLOR,6 + LOOP_CYCLE,10,a(710)
    dta $24,$26,$28,$2a,$28,$26
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_STAR2,12  
    dta BLK_RNDCHAR,CHAR_DUST,12  
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_STAR2,16   
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; -------------------------------------------------- ------ end


    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_PUTTILE,a(baloon),24,6
    dta BLK_PUTTILE,a(baloon),38,9
    dta BLK_PUTTILE,a(baloon),43,14
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; -------------------------------------------------- ------ end 

    dta BLK_CLEAR
    dta BLK_CYCLECOLOR,0,0,a(0)
    dta BLK_SETCOLORS,$0,$d8,$ff
    dta BLK_SETBKSOLID,$72,32
    dta BLK_SLOWDOWN,80
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------


    dta BLK_CLEAR
    dta BLK_PUTTILE,a(word_drink),20,0
    dta BLK_PUTTILE,a(word_drink),37,6
    dta BLK_PUTTILE,a(word_drink),27,12
    dta BLK_SETSTARS,%01110110
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETCOLORS,$76,$7a,$7f
    dta BLK_CYCLECOLOR,6 + LOOP_CYCLE,2,a(710)
    dta $78,$7a,$7c,$7e,$7c,$7a
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY2,8    
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY2,16    
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------  -------  end 

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY2,4    
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_PUTTILE,a(word_think),36,0
    dta BLK_PUTTILE,a(word_think),18,5
    dta BLK_PUTTILE,a(word_think),26,11
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETCOLORS,$76,$7a,$7f
    dta BLK_CYCLECOLOR,8 + LOOP_CYCLE,4,a(710)
    dta $18,$38,$58,$78,$98,$b8,$d8,$f8
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; -------------------------------------------------- -------  end 

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY1,24    
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY1,12    
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; -------------------------------------------------- 

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY1,8    
    dta BLK_RNDCHAR,CHAR_ALLOY2,2    
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY1,4    
    dta BLK_RNDCHAR,CHAR_ALLOY2,4    
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; -------------------------------------------------- -------  end 


    dta BLK_CLEAR
    dta BLK_PUTTILE,a(word_repeat),36,0
    dta BLK_PUTTILE,a(word_repeat),19,5
    dta BLK_PUTTILE,a(word_repeat),25,11
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_STAR2,12   
    dta BLK_CYCLECOLOR,6 + LOOP_CYCLE,6,a(710)
    dta $c4,$c6,$c8,$ca,$c8,$c6
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------


    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY2,12    
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_PUTTILE,a(satelite1),22,9
    dta BLK_PUTTILE,a(satelite2),39,13
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; -------------------------------------------------- 


    dta BLK_CLEAR
    dta BLK_SLOWDOWN,80
    dta BLK_SETCOLORS,$94,$98,$f
    dta BLK_CYCLECOLOR,6 + LOOP_CYCLE,20,a(710)
    dta $0,$2c,$0,$bc,$0,$7c
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; -------------------------------------------------- -------  end 

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY2,24   
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETCOLORS,$72,$7a,$7f
    dta BLK_RNDCHAR,CHAR_ALLOY2,16   
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; -------------------------------------------------- ------ end 3

   
    dta BLK_CLEAR
    dta BLK_SETSTARS,%01010010
    dta BLK_RNDCHAR,CHAR_STAR2,8   
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETPRIO,$22
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETCOLORS,$76,$7a,$7c    
    dta BLK_LOADLINES,a(inv_graw),a(5*$80),2  
    dta BLK_LOADLINES,a(inv_8bit),a(3*$80),2  
    dta BLK_PUTTILE,a(astronaut),29,14
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_CYCLECOLOR,6 + LOOP_CYCLE,4,a(710)
    dta $0a,$0b,$0c,$0e,$0c,$0b    
    dta BLK_SLOWDOWN,150
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; -------------------------------------------------- ------ end 3

    dta BLK_CLEAR
    dta BLK_SETSTARS,%00000000
    dta BLK_RNDCHAR,CHAR_FIREBALL,4
    dta BLK_SETBKTRANS,$72,$64,26
    dta BLK_SETBKSOLID,$64,6
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETPRIO,$24
    dta BLK_SETCHARS,>TILES1
    dta BLK_SETCOLORS,$16,$18,$1a
    dta BLK_RNDCHAR,CHAR_FIREBALL,8
    dta BLK_SETBKSOLID,$64,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_FIREBALL,8
    dta BLK_RNDCHAR,CHAR_FIREBALL,8
    dta BLK_SETBKSOLID,$64,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_FIREBALL,8
    dta BLK_RNDCHAR,CHAR_FIREBALL,8
    dta BLK_RNDCHAR,CHAR_FIREBALL,8
    dta BLK_SETBKSOLID,$64,32
    dta BLK_END  ; -------------------------------------------------- ------  end 4

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_FIREBALL,8
    dta BLK_RNDCHAR,CHAR_FIREBALL,8
    dta BLK_SETBKSOLID,$64,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETSTARS,%01010010
    dta BLK_RNDCHAR,CHAR_FIREBALL,6
    dta BLK_RNDCHAR,CHAR_ALLOY2,6
    dta BLK_SETBKSOLID,$64,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY2,4
    dta BLK_RNDCHAR,CHAR_STAR2,4
    dta BLK_SETBKSOLID,$64,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETSTARS,%10101010
    dta BLK_RNDCHAR,CHAR_STAR2,4
    dta BLK_SETBKSOLID,$64,18
    dta BLK_SETBKTRANS,$64,$54,14
    dta BLK_END  ; -------------------------------------------------- ------  end 4

    dta BLK_CLEAR
    dta BLK_SETCOLORS,$76,$7a,$7f
    dta BLK_SETBKSOLID,$54,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETPRIO,$22
    dta BLK_SETCHARS,>TILES2
    dta BLK_SETSTARS,%11011011
    dta BLK_SETBKSOLID,$54,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_LOADLINES,a(inv_date),a(2*$80),2  
    dta BLK_LOADLINES,a(inv_krakow),a(5*$80),3  
    dta BLK_PUTTILE,a(sputnik),30,7
    dta BLK_SETBKSOLID,$54,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETSTARS,%11111111
    dta BLK_SETBKSOLID,$54,32
    dta BLK_SLOWDOWN,150
    dta BLK_END  ; -------------------------------------------------- ------  end 5

    dta BLK_CLEAR
    dta BLK_SETSTARS,%11011011
    dta BLK_SETBKSOLID,$54,32
    dta BLK_END  ; --------------------------------------------------

    
    dta BLK_CLEAR
    dta BLK_SETPRIO,$24
    dta BLK_SETCHARS,>TILES1
    dta BLK_SETCOLORS,$d6,$c8,$ba
    dta BLK_CYCLECOLOR,6 + LOOP_CYCLE,12,a(710)
    dta $c8,$ca,$cc,$ca,$c8,$c6       
    dta BLK_SETSTARS,%10101010
    dta BLK_SETBKSOLID,$54,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETSTARS,%00100010
    dta BLK_SETBKSOLID,$54,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETSTARS,%00100000
    dta BLK_SETBKSOLID,$54,32
    dta BLK_END  ; -------------------------------------------------- ------  end 5

    dta BLK_CLEAR
    dta BLK_RNDTILE,a(cloud0),6
    dta BLK_RNDTILE,a(cloud1),2
    dta BLK_SETSTARS,%01001001
    dta BLK_SETBKTRANS,$54,$52,26
    dta BLK_SETBKSOLID,$52,6
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDTILE,a(cloud0),2
    dta BLK_RNDTILE,a(cloud1),2
    dta BLK_RNDTILE,a(cloud2),1
    dta BLK_RNDTILE,a(cloud3),1
    dta BLK_RNDTILE,a(cloud4),1
    dta BLK_SETBKTRANS,$52,$62,26
    dta BLK_SETBKSOLID,$62,6
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDTILE,a(cloud0),4
    dta BLK_RNDTILE,a(cloud1),2
    dta BLK_RNDTILE,a(cloud2),2
    dta BLK_RNDTILE,a(cloud3),2
    dta BLK_RNDTILE,a(cloud4),2
    dta BLK_SETBKTRANS,$62,$64,26
    dta BLK_SETBKSOLID,$64,6
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDTILE,a(cloud0),2
    dta BLK_RNDTILE,a(cloud1),2
    dta BLK_RNDTILE,a(cloud2),1
    dta BLK_RNDTILE,a(cloud3),1
    dta BLK_RNDTILE,a(cloud4),1
    dta BLK_SETSTARS,%01101011
    dta BLK_SETBKTRANS,$64,$72,26
    dta BLK_SETBKSOLID,$72,6
    dta BLK_END  ; --------------------------------------------------  --- end

    dta BLK_CLEAR
    dta BLK_RNDTILE,a(cloud0),6
    dta BLK_RNDTILE,a(cloud1),2
    dta BLK_SETSTARS,%01111101
    dta BLK_SETBKSOLID,$72,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKTRANS,$72,$70,26
    dta BLK_SETBKSOLID,$70,6
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETPRIO,$22
    dta BLK_SETSTARS,%10111111
    dta BLK_SETBKSOLID,$70,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETSTARS,%11111111
    dta BLK_SETBKSOLID,$70,32
    dta BLK_END  ; --------------------------------------------------  end

    dta BLK_CLEAR
    dta BLK_LOADLINES,a(inv_more),a(2*$80),2  
    dta BLK_LOADLINES,a(inv_www),a(4*$80),2  
    dta BLK_SETBKSOLID,$70,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETSTARS,%11111111
    dta BLK_SETBKSOLID,$70,32
    dta BLK_SLOWDOWN,150
    dta BLK_END  ; -------------------------------------------------- 

    dta BLK_CLEAR
    dta BLK_SETSTARS,%11011011
    dta BLK_SETBKSOLID,$70,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETPRIO,$24
    dta BLK_SETCHARS,>TILES1
    dta BLK_CYCLECOLOR,0,0,a(0)
    dta BLK_SETCOLORS,$0a,$00,$06
    dta BLK_SETSTARS,%00100010
    dta BLK_SETBKSOLID,$70,6
    dta BLK_SETBKTRANS,$70,$80,26
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,16
    dta BLK_RNDTILE,a(meteor0),5
    dta BLK_SETBKSOLID,$80,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,24
    dta BLK_RNDTILE,a(meteor0),8
    dta BLK_SETBKSOLID,$80,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,32
    dta BLK_RNDTILE,a(meteor0),8
    dta BLK_RNDTILE,a(meteor1),5
    dta BLK_RNDTILE,a(meteor2),2
    dta BLK_SETBKSOLID,$80,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,16
    dta BLK_RNDTILE,a(meteor0),12
    dta BLK_RNDTILE,a(meteor1),3
    dta BLK_RNDTILE,a(meteor2),4
    dta BLK_SETBKSOLID,$80,32
    dta BLK_END  ; --------------------------------------------------


    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,8
    dta BLK_RNDTILE,a(meteor0),6
    dta BLK_SETBKSOLID,$80,6
    dta BLK_SETBKTRANS,$80,$90,26
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,4
    dta BLK_SETSTARS,%00101010
    dta BLK_SETBKSOLID,$90,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$90,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$90,32
    dta BLK_SETSTARS,%10101010
    dta BLK_END  ; --------------------------------------------------



    
    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,4
    dta BLK_SETCHARS,>TILES2
    dta BLK_SETCOLORS,$b2,$c8,$bf
    dta BLK_CYCLECOLOR,6 + LOOP_CYCLE,20,a(710)
    dta $f,$2c,$f,$bc,$f,$7c
    dta BLK_SETBKSOLID,$90,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,16
    dta BLK_PUTTILE,a(ufo1),20,8
    dta BLK_PUTTILE,a(ufo2),38,13
    dta BLK_SETBKSOLID,$90,6
    dta BLK_SETBKTRANS,$90,$92,26
    dta BLK_SETSTARS,%00100010
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,32
    dta BLK_PUTTILE,a(ufo1),42,7
    dta BLK_PUTTILE,a(ufo2),22,12
    dta BLK_PUTTILE,a(rays),23,13
    dta BLK_SETBKSOLID,$92,32
    dta BLK_SLOWDOWN,180
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,16
    dta BLK_SETBKSOLID,$92,32
    dta BLK_END  ; --------------------------------------------------


    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$92,32
    dta BLK_END  ; --------------------------------------------------


    dta BLK_CLEAR
    dta BLK_SETSTARS,%0010000
    dta BLK_CYCLECOLOR,0,0,a(0)
    dta BLK_SETCOLORS,$66,$6a,$0
    dta BLK_SETBKSOLID,$92,6
    dta BLK_RNDCHAR,CHAR_ALLOY2,8
    dta BLK_SETBKTRANS,$92,$84,26
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY2,8
    dta BLK_RNDCHAR,CHAR_STAR2,8
    dta BLK_SETBKSOLID,$84,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY2,12
    dta BLK_RNDCHAR,CHAR_STAR2,12
    dta BLK_SETBKSOLID,$84,6
    dta BLK_SETBKTRANS,$84,$86,26
    dta BLK_END  ; --------------------------------------------------


    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_ALLOY2,16
    dta BLK_RNDCHAR,CHAR_STAR2,16
    dta BLK_SETBKSOLID,$86,32
    dta BLK_SETSTARS,%00100100
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,16
    dta BLK_RNDCHAR,CHAR_ALLOY2,16
    dta BLK_RNDCHAR,CHAR_STAR2,16
    dta BLK_SETBKSOLID,$86,6
    dta BLK_SETBKTRANS,$86,$78,26
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,25
    dta BLK_RNDCHAR,CHAR_ALLOY2,12
    dta BLK_RNDCHAR,CHAR_STAR2,12
    dta BLK_SETBKSOLID,$78,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,16
    dta BLK_RNDCHAR,CHAR_ALLOY2,8
    dta BLK_RNDCHAR,CHAR_STAR2,8
    dta BLK_SETBKSOLID,$78,6
    dta BLK_SETBKTRANS,$78,$7a,26
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,12
    dta BLK_RNDCHAR,CHAR_ALLOY2,6
    dta BLK_RNDCHAR,CHAR_STAR2,6
    dta BLK_SETBKSOLID,$7a,32
    dta BLK_SETSTARS,%10100100
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,8
    dta BLK_RNDCHAR,CHAR_ALLOY2,4
    dta BLK_RNDCHAR,CHAR_STAR2,4
    dta BLK_SETBKSOLID,$7a,6
    dta BLK_SETBKTRANS,$7a,$7c,26
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_RNDCHAR,CHAR_DUST,6
    dta BLK_SETBKSOLID,$7c,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$7c,6
    dta BLK_SETBKTRANS,$7c,$7f,26
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$7f,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$7f,6
    dta BLK_SETBKTRANS,$7f,$0f,26
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$f,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$f,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$f,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_LOADLINES,a(inv_www),a(6*$80),2  
    dta BLK_SETBKSOLID,$f,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$f,32
    dta BLK_SLOWDOWN,100;
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_EXPLODE
    dta BLK_SETBKSOLID,$f,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$f,32
    dta BLK_END  ; --------------------------------------------------

    dta BLK_CLEAR
    dta BLK_SETBKSOLID,$f,32
    dta BLK_END  ; --------------------------------------------------


    
    dta BLK_JUMP, a(loop_point)
    
level2

.print '*** Level size1: ',*-level1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;        TILES 1

cloud0
  dta 5,1
  dta 33,34,37,37,33
  
cloud1  
  dta 10,1
  dta 33,37,37,38,38, 38,36,36,32,32
  
cloud2
  dta 13,1
  dta 33,33,34,34,32, 32,34,34,34,34, 33,33,33
  
cloud3
  dta 13,1
  dta 33,33,34,34,38, 38,38,37,37,37, 35,35,35
  
cloud4
  dta 19,1
  dta 32,34,34,38,34, 38,34,38,38,38, 38,38,38,38,38, 34,33,33,33

meteor0
    dta 2,1
    dta 44,45
    
meteor1
    dta 2,1
    dta 46,47

meteor2
    dta 3,1
    dta 48,49,50


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;        TILES 1  

satelite1
    dta 4,2
    dta 12,1,2,3
    dta 32,33,34,35
    
sputnik    
    dta 4,2
    dta 4,5,6,7
    dta 36,37,38,31
        
satelite2
    dta 7,2
    dta 0,0,10,11,8,9,63
    dta 8,9,63,43,44,0,0
    
baloon    
    dta 3,2
    dta 13,14,15
    dta 45,46,47

ufo1    
    dta 5,1
    dta 20,21,22,23,24

ufo2    
    dta 5,1
    dta 52,53,53,53,54
    
rays
    dta 3,1
    dta 55,56,57
    
moon    
    dta 6,3
    dta 0,0,0,25,26,27
    dta 0,0,0,0,58,59
    dta 28,29,30,60,61,62

astronaut  
    dta 5,2
    dta 16,17,18,19,35
    dta 48,49,50,51,0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; BUZZZZZ WORDS

word_code
    dta 8,2
    dta 68,102,68,69,120,121,76,77
    dta 100,105,100,101,108,101,108,109

word_eat  
    dta 6,2
    dta 76,77,78,69,64,65
    dta 108,109,98,98,96,0

word_think  
    dta 10,2
    dta 64,65,74,75,86,87,82,75,116,117
    dta 96,0,98,98,118,119,98,98,98,98

word_drink  
    dta 10,2
    dta 120,121,66,67,86,87,82,75,116,117
    dta 108,101,98,98,118,119,98,98,98,98

word_repeat  
    dta 12,2
    dta 66,67,76,77,70,71,76,77,78,69,64,65
    dta 98,98,108,109,98,0,108,109,98,98,96,0

word_compete  
    dta 14,2
    dta 68,102,68,69,80,81,70,71,76,77,64,65,76,77
    dta 100,105,100,101,98,113,98,0,108,109,96,0,108,109


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    landscape    

landing
  ;; RLE encoded data
  dta $32, $00, $09, $21, $22, $25, $25, $21, $fc, $00, $7e, $00, $07, $20, $21, $20
  dta $20, $04, $22, $04, $25, $0b, $26, $26, $25, $25, $22, $22, $04, $21, $ba, $00
  dta $0b, $21, $21, $22, $22, $20, $20, $06, $22, $04, $21, $f8, $00, $0b, $1a, $1b
  dta $1c, $1d, $1e, $1f, $06, $01, $13, $1f, $33, $34, $35, $36, $37, $00, $21, $25
  dta $25, $04, $26, $07, $24, $24, $20, $20, $42, $00, $05, $17, $18, $19, $1e, $01
  dta $05, $38, $39, $3a, $4e, $00, $03, $15, $16, $2a, $01, $03, $3b, $3c, $48, $00
  dta $01, $14, $22, $01, $05, $0f, $0f, $10, $08, $01, $01, $3d, $44, $00, $07, $13
  dta $01, $01, $06, $1e, $01, $09, $08, $0d, $0e, $0f, $10, $06, $01, $01, $3e, $40
  dta $00, $01, $12, $04, $01, $0f, $04, $04, $05, $01, $0b, $0c, $0b, $0c, $0a, $01
  dta $0f, $0f, $11, $0f, $08, $0d, $0e, $0d, $0e, $06, $01, $03, $06, $3f, $3e, $00
  dta $01, $08, $04, $01, $0f, $04, $04, $05, $01, $04, $02, $04, $02, $0e, $01, $17
  dta $0d, $08, $0d, $0e, $0d, $0e, $01, $01, $0a, $04, $08, $07, $1c, $00, $22, $02
  dta $0d, $0b, $0c, $01, $04, $04, $05, $03, $06, $02, $05, $03, $03, $11, $04, $02
  dta $11, $11, $03, $0d, $08, $0d, $0e, $0d, $0e, $09, $a6, $02, $00


;;;;;;;;;;;;;;;;;; INVITATIONS

inv_graw
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,88,89,66,67,78,69,75,83,86,87,64,65,78,69,68,102,99,75,78,69,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,98,98,98,98,98,114,115,118,119,96,0,98,98,100,105,124,101,98,98,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
inv_8bit
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,112,0,126,67,112,66,67,86,87,64,65,0,88,89,78,69,80,81,76,77,99,75,78,69,80,81,0,112,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,109,109,0,100,101,0,108,101,118,119,96,0,0,100,98,98,98,98,113,108,109,124,101,98,98,98,113,0,109,109,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
inv_date
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,90,91,0,0,127,69,0,86,0,68,69,0,122,123,68,69,122,123,122,123,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,98,0,99,0,124,101,109,118,119,100,101,109,108,109,100,101,108,109,108,109,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
inv_krakow
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,112,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,116,117,66,67,78,69,116,117,68,69,75,83,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,98,98,98,98,98,98,98,98,100,101,114,115,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
inv_more
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,80,81,68,69,66,67,76,77,0,86,87,82,75,76,77,68,69,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,98,113,100,101,98,98,108,109,0,118,119,98,98,98,0,100,101,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
inv_www
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,88,89,66,67,78,69,75,83,86,87,64,65,78,69,68,102,99,75,78,69,0,76,77,75,75,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    dta 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,98,98,98,98,98,114,115,118,119,96,0,98,98,100,105,124,101,98,98,109,108,109,100,101,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0






.print '*** All Levels Size: ',*-levels

