import sys

infile = sys.argv[1]
outfile = sys.argv[2]
tsWidth = int(sys.argv[3])

outBytes = []
count = 0


with open(infile, "rb") as f:
    
    rowsize = tsWidth * 8
    inbytes = f.read(rowsize)

    while inbytes and (count<1024):
        for x in range(rowsize):
            yoff = (x * tsWidth) % (8 * tsWidth);
            xoff = (x//8)
            outBytes.append(inbytes[yoff + xoff]);   
            count += 1
        inbytes = f.read(rowsize)

print('bytes converted:', count)

with open(outfile, "wb") as f:
    f.write(bytearray(outBytes))


    


