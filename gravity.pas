program madStrap;
{ $librarypath '../blibs/'}
uses atari, crt, rmt, joystick, graph; // b_utils;

const
{$i const.inc}
BOTTOM_BORDER = (((VRAM_SIZE div MAP_WIDTH) - SCREEN_HEIGHT) * 16);
BLOCK_SIZE = VRAM_SIZE div 4;
BLOCK_HEIGHT = 16;

{$r resources.rc}
{$i interrupts.inc}

var
    b,i: byte;
    c:char;
    src,dest: word;
    hscroll: byte;
    viewX:word;
    viewY:word;
    ship0: array[0..SHIP_FRAMES-1,0..SHIP_HEIGHT-1] of byte absolute SPRITES_ADDRESS;
    ship1: array[0..SHIP_FRAMES-1,0..SHIP_HEIGHT-1] of byte absolute SPRITES_ADDRESS + (SHIP_FRAMES * SHIP_HEIGHT);
    levels: array[0..0] of word absolute LEVELS_ADDRESS;

    colorCycleCounter: byte;
    colorCycleLen: byte;
    colorCycleLoop: boolean;
    colorCycleSpeed: byte;
    cycleColor: word;
    colorCycleDelay: byte;
    cycleColors: array [0..31] of byte;
    explode: byte;
    music_on: byte;
    
    xdir: byte;
    xstep: byte;
    xforce: byte;
    yforce: byte;
    ystep: byte;


    shipframe: byte;
    shipY: word;
    dliY: byte;
    yinc: byte;
    dx,dy:byte;
    dliback: array [0..255] of byte absolute DLIBACK_ADDRESS;
    starsPos: array [0..255] of byte absolute STARS_ADDRESS;
    dlibackOffset: byte;
    dlibackWrite: byte;
    lastStars: byte;
    offStep: byte;
    starMask: byte;
    startOffset: byte;
    
    b16Num:byte;
    windowNum:byte;
    lastblock:word;
    generatingStarted:boolean;

    levelPtr:word;
    blockWait:byte;
    starsColor:byte;
    slowDown:byte;
    
    randomChar :byte;
    randomCount :byte;
    randomTile :word;
    randomRoot :word;

    msx: TRMT;


procedure PlaySong(p:byte);
begin
    msx.Init(p);
    music_on := 1;
end;

procedure StopSong;
begin
    pause();
    msx.Stop;
    music_on := 0;
end;

procedure ExpandRLE(src: word; dest: word);
var value, count:byte;
begin
    value := peek(src);
    while value<>0 do begin
        Inc(src);
        count := (value shr 1) + 1;
        if Odd(value) then begin // just repeat data
            Move(pointer(src),pointer(dest),count);
            Inc(src,count);
        end else begin  // expand
            FillChar(pointer(dest),count,peek(src));
            Inc(src);
        end;
        Inc(dest,count);
        value := peek(src);
    end;
end; 

procedure writeDli(c,count:byte);
begin
    while count>0 do begin
        poke(DLIBACK_ADDRESS + dlibackWrite, c);
        dec(dlibackWrite);
        dec(count);
    end;
end;
    
procedure generateBlockStep();
var cmd:byte;
    bparam1:byte;
    bparam2:byte;
    bparam3:byte;
    wparam1:word;
    wparam2:word;
    height,width:byte;
begin

    if blockWait = 0 then begin

        dest := VIDEO_RAM_ADDRESS;
        for i:=1 to windowNum do dest := dest + BLOCK_SIZE;

        cmd := peek(levelPtr);
        Inc(levelPtr,1);
        case (cmd) of
            
            BLK_CLEAR: begin
                fillByte(pointer(dest),BLOCK_SIZE,0);
            end;
            
            BLK_END: begin
                windowNum := (windowNum - 1) and 3;
                Inc(b16Num);        
                generatingStarted := false;
            end;
            
            BLK_SETCOLORS: begin
                color0 := peek(levelPtr);
                color1 := peek(levelPtr+1);
                color2 := peek(levelPtr+2);
                Inc(levelPtr,3);
            end;
            
            BLK_LOADRAW: begin
                wparam1 := dpeek(levelPtr);
                move(pointer(wparam1),pointer(dest),BLOCK_SIZE);
                Inc(levelPtr,2);
            end;

            BLK_LOADRLE: begin
                wparam1 := dpeek(levelPtr);
                ExpandRLE(wparam1,dest);
                Inc(levelPtr,2);
            end;
            
            BLK_LOADLINES : begin
                wparam1 := dpeek(levelPtr); // address
                wparam2 := dpeek(levelPtr+2); // offset
                bparam1 := peek(levelPtr+4); // line count
                move(pointer(wparam1),pointer(dest+wparam2),bparam1 shl 6);
                Inc(levelPtr,5);
            end;
                        
            BLK_RNDCHAR: begin
                randomChar := peek(levelPtr);
                randomCount := peek(levelPtr+1);
                randomTile := 0;
                randomRoot := dest;
                Inc(levelPtr,2);
            end;
            
            BLK_RNDTILE: begin
                randomChar := 0;
                randomCount := peek(levelPtr+2);
                randomTile := dpeek(levelPtr);
                randomRoot := dest;
                Inc(levelPtr,3);
            end;

            BLK_PUTTILE: begin
                dest := dest + peek(levelPtr+2) + (peek(levelPtr+3) shl 6);
                src := dpeek(levelPtr);
                height := peek(src+1);
                width := peek(src);
                inc(src,2);
                for i:=0 to height-1 do begin
                    move(pointer(src), pointer(dest), width);
                    inc(src,width);
                    inc(dest,MAP_WIDTH);
                end;
                Inc(levelPtr,4);
            end;
            
            BLK_JUMP: begin
                wparam1 := dpeek(levelPtr);
                levelPtr := wparam1;
            end; 
            
            BLK_SETCHARS: begin
                chbas := peek(levelPtr);
                Inc(levelPtr,1);
            end;

            BLK_SETX: begin
                viewX := peek(levelPtr);
                Inc(levelPtr,1);
            end;

            BLK_SETPRIO: begin
                GPRIOR := peek(levelPtr);
                Inc(levelPtr,1);
            end;

            BLK_STARTOFF: begin
                startOffset := peek(levelPtr);
                Inc(levelPtr,1);
            end;

            BLK_SETBKSOLID: begin
                bparam1 := peek(levelPtr);
                bparam2 := peek(levelPtr+1);
                while bparam2 > 0 do begin
                    poke(DLIBACK_ADDRESS + dlibackWrite, bparam1);
                    dec(dlibackWrite);
                    dec(bparam2)
                end;
                Inc(levelPtr,2);
            end;
            
            BLK_SLOWDOWN: begin
                slowDown := peek(levelPtr);
                Inc(levelPtr,1);
            end;
            
            BLK_SETBKTRANS: begin
                bparam1 := peek(levelPtr);
                bparam2 := peek(levelPtr+1);
                bparam3 := peek(levelPtr+2);

                if bparam3 = 26 then begin
                    writeDli(bparam2,1);
                    writeDli(bparam1,3);
                    writeDli(bparam2,1);
                    writeDli(bparam1,2);
                    writeDli(bparam2,1);
                    writeDli(bparam1,2);
                    writeDli(bparam2,1);
                    writeDli(bparam1,1);
                    writeDli(bparam2,1);
                    writeDli(bparam1,1);
                    writeDli(bparam2,1);
                    writeDli(bparam1,1);
                    writeDli(bparam2,2);
                    writeDli(bparam1,1);
                    writeDli(bparam2,2);
                    writeDli(bparam1,1);
                    writeDli(bparam2,3);
                    writeDli(bparam1,1);
                end;
    
                if bparam3 = 14 then begin
                    writeDli(bparam2,1);
                    writeDli(bparam1,4);
                    writeDli(bparam2,2);
                    writeDli(bparam1,2);
                    writeDli(bparam2,4);
                    writeDli(bparam1,1);
                end;
                
                if bparam3 = 6 then begin
                    writeDli(bparam2,1);
                    writeDli(bparam1,2);
                    writeDli(bparam2,2);
                    writeDli(bparam1,1);
                end;
                
                if bparam3 = 2 then begin
                    poke(DLIBACK_ADDRESS + dlibackWrite, bparam2);
                    dec(dlibackWrite);
                    poke(DLIBACK_ADDRESS + dlibackWrite, bparam1);
                    dec(dlibackWrite);
                end;
                
                Inc(levelPtr,3);
            end;

            BLK_SETSTARS: begin
                starMask := peek(levelPtr);
                Inc(levelPtr,1);
            end;
            
            BLK_WAIT: begin
                blockWait := peek(levelPtr);
                Inc(levelPtr,1);
            end;
                                            // params
            BLK_CYCLECOLOR: begin           // len (0=off), speed, memory, array [0..len-1] values....
                colorCycleLen := peek(levelPtr) and %01111111;
                colorCycleLoop := (peek(levelPtr) and LOOP_CYCLE) <> 0;
                colorCycleSpeed := peek(levelPtr+1);
                cycleColor := dpeek(levelPtr+2);
                colorCycleDelay := 0;
                colorCycleCounter := 0;
                Inc(levelPtr,4);
                for i:=1 to colorCycleLen do begin
                    cycleColors[i-1] := peek(levelPtr);
                    Inc(levelPtr);
                end;
            end;

            BLK_EXPLODE: begin
                explode := 1;
                StopSong;
                PlaySong(16);
            end;
    
        end;
    end else dec(blockWait);
end;

procedure generateBlock;
begin
    generatingStarted := true;
    repeat 
        generateBlockStep();
    until not generatingStarted;
end;

    
procedure randomizeLevelStep;   
var c:byte;
    src,dest:word;
    x,y:byte;
    width,height:byte;
begin
    if randomChar <> 0 then begin
        c := 16;
        if randomCount < c then c := randomCount;
        dec(randomCount, c);
        while (c > 0) do begin
            dest := ((Random(255) and 3) shl 8) + Random(255);
            dest := dest + randomRoot;
            poke(dest, randomChar);
            dec(c);
        end;
        if randomCount = 0 then randomChar := 0;
    end;
    if randomTile <> 0 then begin
        width := peek(randomTile);
        height := peek(randomTile+1);
        c := 8;
        if randomCount < c then c := randomCount;
        dec(randomCount, c);
        while (c > 0) do begin
            x := Random(byte(MAP_WIDTH-width));
            y := Random(byte(BLOCK_HEIGHT-height));
            dest := randomRoot + x + (y shl 6);
            src := randomTile+2;
            for y:=0 to height-1 do begin
                move(pointer(src),pointer(dest),width);
                inc(src,width);
                inc(dest,MAP_WIDTH);
            end;
            dec(c);
        end;
        if randomCount = 0 then randomTile := 0;
    end;
end;
    
procedure setView();
begin
    if lastblock<>Hi(viewY) then begin
        generatingStarted := true;
    end;
    
    dest := VIDEO_RAM_ADDRESS + (((viewY and 1023)shr 4) shl 6) + (viewX shr 2);
    hscroll := 15 - (viewX and 3);
    src:=DISPLAY_LIST_ADDRESS + 4;
    for i:=0 to 12 do begin
        if dest < VIDEO_RAM_ADDRESS then dest := dest + VRAM_SIZE;
        if dest >= VIDEO_RAM_ADDRESS + VRAM_SIZE then dest := dest - VRAM_SIZE;
        Dpoke(src,dest);

        Inc(src,3);
        Inc(dest,MAP_WIDTH);
    end;
    hscrol := hscroll;
    vscrol := (viewY and 14);
    lastblock := Hi(viewY);
end;

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

begin
   
    Randomize;
    color4:=$0;
    color0:=$78;
    color1:=$7a;
    color2:=$7c;
    starsColor:=$7a;
    explode := 0;
    
    msx.player := pointer(RMT_PLAYER_ADDRESS);
    msx.modul := pointer(RMT_MODULE_ADDRESS);    
    music_on := 0;
        
    for i:=0 to 127 do poke(PMG + $180 + i,$10);
    
    randomCount := 0;
    viewX := ((MAP_WIDTH - SCREEN_WIDTH) shl 1)+3;
    viewY := BOTTOM_BORDER + 1;
    lastblock := Hi(viewY);
    blockWait := 0;
    setView;

    pcolr0 := $14;
    pcolr1 := $18;
    sizep0 := 0;
    sizep1 := 0;

    xdir := MID;
    xstep := 0;
    xforce := 0;
    ystep := 0;
    yforce := 0;

    b16Num := 0;
    windowNum := 3;

    levelPtr := levels[0];
       

    dlibackOffset := 255 - 96;
    lastStars := dlibackOffset;
    dlibackWrite := 0;
    starMask := 0;

    Pause;
    nmien := $0; 
    SetIntVec(iVBL, @vbl);
    nmien := $40; // set $80 for dli only (without vbl)


    InitGraph(8+16);

    PlaySong(0);

    color2:=0;
    color1:=0;
    ExpandRLE(HIR4,savmsc);
    
    Pause(200);
    Pause(100);
    for i:=0 to 30 do begin
        color1 := i shr 1;
        pause;
    end;

    Pause(200);
    Pause(100);
    
    ExpandRLE(FMU,savmsc + 40*130);

    Pause(200);
    Pause(100);

    Pause(200);
    Pause(100);

    for i:=30 downto 0 do begin
        color1 := i shr 1;
        pause;
    end;

    Pause;
    nmien := $0; 
    SDLSTL := DISPLAY_LIST_ADDRESS;    
    SDMCTL := %00101101;    
    GPRIOR := $24;    
    PMBASE := Hi(PMG);
    GRACTL := %00000011;

    SetIntVec(iDLI, @dli);
    SetIntVec(iVBL, @vbl);
    nmien := $c0; // set $80 for dli only (without vbl)
    hscrol := 15 - (viewX and 3);

    generateBlock();
    generateBlock();
    
    StopSong;
    
    repeat

        if randomCount > 0 then begin
            randomizeLevelStep();
        end else 
            if generatingStarted then generateBlockStep();

        shipframe := ((rtclok3 shr 1) and 3) + 4;

        b := (not stick0 and 15);
        
        if (b and 12) = 0 then begin                   // h mid
            xdir := MID;
            if xforce > 0 then dec(xforce);
        end;
                
        if (b and 8) <> 0 then                         // right
            if viewX < RIGHT_BORDER then begin
                if xdir = RIGHT then begin
                    if xforce < yforce shr 3 then inc(xforce);
                end else begin
                    xdir := RIGHT;
                    xforce := 1;
                    xstep := 4;
                end;
            end else xdir := LEFT; // this one gives a bounce effect
            
        if (b and 4) <> 0 then                        // left
            if viewX > 0 then begin
                if xdir = LEFT then begin
                    if xforce < yforce shr 3 then inc(xforce);
                end else begin
                    xdir := LEFT;
                    xforce := 1;
                    xstep := 4;
                end;
            end else xdir := RIGHT; // this one gives a bounce effect

        xstep := xstep + xforce;
        
        if xstep>3 then begin
            if xdir = RIGHT then begin 
                inc(viewX, xstep shr 2);
                xstep := xstep and 3;
                if viewX > RIGHT_BORDER then viewX := RIGHT_BORDER;
            end;
            if xdir = LEFT then begin 
                dec(viewX, xstep shr 2);
                xstep := xstep and 3;
                if viewX > RIGHT_BORDER then viewX:=0;
            end;
        end;

        if xforce > 6 then shipframe := shipframe + 4;
    
            
        if (b and 3) = 0 then begin             // v-mid
            if yforce > 24 then dec(yforce);
            if (yforce > 0) and (yforce < 24) then inc(yforce);
        end;

        if (b and 1)<>0 then begin                 // up;
            if yforce < 64 then inc(yforce);
            shipframe := shipframe + 8;
            if music_on = 0 then PlaySong(6);
        end;
            
        if (b and 2) <> 0 then begin            // down
            if yforce > 16 then dec(yforce);
            shipframe := shipframe - 4;
        end;

        if yforce = 0 then shipframe := 1;
        
        yinc := yforce;
        if slowDown > 0 then begin
                yinc := 16;
                dec(slowDown);
        end;
        
        ystep := ystep + yinc;
        
        if ystep > 7 then begin
            yinc := ystep shr 3;
            dec(viewY, yinc);
            inc(offStep, yinc);
            ystep := ystep and 7;
            if offStep > 7 then begin
                dec(dlibackOffset, offStep shr 3);
                offStep := offStep and 7;
                if startOffset > 0 then dec(startOffset);
            end;
        end;
                    
        while dlibackOffset<>lastStars do begin
            b := 1 shl (lastStars and 7);
            if (b and starMask) <> 0 then b:=Random(255) else b:=0;
            starsPos[lastStars] := b;
            dec(lastStars);
        end;
        
        pause;
        setView();
        
        if colorCycleLen > 0 then begin
            if colorCycleDelay = 0 then begin
                colorCycleDelay := colorCycleSpeed;
                poke(cycleColor, cycleColors[colorCycleCounter]);
                inc(colorCycleCounter);
                if colorCycleCounter = colorCycleLen then 
                    if colorCycleLoop then 
                        colorCycleCounter := 0
                    else
                        colorCycleLen := 0
            end else 
                dec(colorCycleDelay);
        end;

        dx := 2;
        dy := 96;
                        
        if yforce = 64 then begin// shake that body!
            if random(2) = 0 then 
                if random(5) = 0 then dec(dy) else inc(dy);
            if random(2) = 0 then 
                if random(5) = 0 then dec(dx) else inc(dx);
        end;
        
        shipY := dy - (yforce shr 1) - startOffset + (explode shr 1);  
        dliY := PMG_TOP + shipY + 4;
        shipY := shipY + PMG + $200 + PMG_TOP;

        if explode > 0 then begin
            shipframe := $13 + (explode shr 2);
            inc(explode);
        end;
        
        if explode < 40 then begin 
            move(@ship0[shipframe, 0], pointer(shipY), SHIP_HEIGHT);
            poke(shipY-1,0);
            poke(shipY-2,0);
            poke(shipY+SHIP_HEIGHT+1,0);
            inc(shipY,$80);
            move(@ship1[shipframe, 0], pointer(shipY), SHIP_HEIGHT);
            poke(shipY-1,0);
            poke(shipY-2,0);
            poke(shipY+SHIP_HEIGHT+1,0);
            
            hposp0 := PMG_LEFT + viewX - dx;
            hposp1 := PMG_LEFT + viewX - dx;
        end else begin
            hposp0 := 0;
            hposp1 := 0;
                 
        end;
        

    until explode = 110;
        
    pause;
    textmode(0);
    CursorOff;
    color2 := 0;
    color1 := 0;
    Writeln;
    Writeln;
    Writeln('      You have been watching');
    Writeln('        GRAWITACJA invitro  ');
    Writeln;
    Writeln('   code: bocianu    music: lisu');
    Writeln;
    Writeln;
    Writeln('         greetings to all');
   
    PlaySong(0);
    for i:=0 to 30 do begin
        color1 := i shr 1;
        pause;
    end;

    Pause(120);
    Writeln;
    Writeln;
    Writeln('       pinokio wez to scisz!');

    repeat until false;
        
        
(*  restore system interrupts *)
    //SetIntVec(iVBL, @oldvbl);
    //SetIntVec(iDLI, oldsdli);
    //nmien := $40; // turn off dli

    msx.Play;

end.
